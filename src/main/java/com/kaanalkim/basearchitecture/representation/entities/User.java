package com.kaanalkim.basearchitecture.representation.entities;

import com.kaanalkim.basearchitecture.representation.core.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class User extends BaseEntity {
    @NotNull
    @Size(max = 50)
    private String name;

    @NotNull
    @Size(max = 50)
    private String surname;

    @NotNull
    @Size(max = 100)
    private String code;

    @Size(max = 15)
    private String mobilePhone;

    @Size(max = 10)
    private String internalPhoneCode;

    @NotNull
    @Size(max = 100)
    private String username;

    @NotNull
    @JsonIgnore
    private String password;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getInternalPhoneCode() {
        return internalPhoneCode;
    }

    public void setInternalPhoneCode(String internalPhoneCode) {
        this.internalPhoneCode = internalPhoneCode;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
package com.kaanalkim.basearchitecture.representation.entities;

import com.kaanalkim.basearchitecture.representation.core.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by gunerkaanalkim on 19/09/15.
 */
@Entity
public class RoleMenu extends BaseEntity {
    @ManyToOne
    @JoinColumn(name = "roleOid", referencedColumnName = "oid")
    private Role role;

    @ManyToOne
    @JoinColumn(name = "menuOid", referencedColumnName = "oid")
    private Menu menu;

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }
}

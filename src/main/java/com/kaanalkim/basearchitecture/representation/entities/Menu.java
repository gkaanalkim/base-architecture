package com.kaanalkim.basearchitecture.representation.entities;

import com.kaanalkim.basearchitecture.representation.core.BaseEntity;

import javax.persistence.Entity;

/**
 * Created by gunerkaanalkim on 19/09/15.
 */
@Entity
public class Menu extends BaseEntity {
    private String name;
    private String code;
    private String description;
    private String url;
    private String parentOid;
    private Integer menuOrder;

    public Integer getMenuOrder() {
        return menuOrder;
    }

    public void setMenuOrder(Integer menuOrder) {
        this.menuOrder = menuOrder;
    }

    public String getParentOid() {
        return parentOid;
    }

    public void setParentOid(String parentOid) {
        this.parentOid = parentOid;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

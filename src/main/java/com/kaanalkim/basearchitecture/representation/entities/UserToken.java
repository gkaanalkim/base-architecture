package com.kaanalkim.basearchitecture.representation.entities;

import com.kaanalkim.basearchitecture.representation.core.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class UserToken extends BaseEntity {
    @OneToOne
    @JoinColumn(name = "userOid", referencedColumnName = "oid")
    private User user;
    private String token;
    private Boolean status;
    private String ip;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
}

package com.kaanalkim.basearchitecture.representation.dto;

/**
 * Created by gunerkaanalkim on 26/09/15.
 */
public class UserDTO {
    private String name;
    private String surname;
    private String mobilePhone;
    private String internalPhoneCode;
    private String email;
    private String password;
    private Boolean deleteStatus = false;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getInternalPhoneCode() {
        return internalPhoneCode;
    }

    public void setInternalPhoneCode(String internalPhoneCode) {
        this.internalPhoneCode = internalPhoneCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getDeleteStatus() {
        return deleteStatus;
    }

    public void setDeleteStatus(Boolean deleteStatus) {
        this.deleteStatus = deleteStatus;
    }
}

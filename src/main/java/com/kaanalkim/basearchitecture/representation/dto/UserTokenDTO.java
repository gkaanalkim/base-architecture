package com.kaanalkim.basearchitecture.representation.dto;

import org.joda.time.DateTime;

public class UserTokenDTO {
    private String username;
    private DateTime dateTime;
    private String ip;

    public UserTokenDTO(String username, DateTime dateTime, String ip) {
        this.username = username;
        this.dateTime = dateTime;
        this.ip = ip;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public DateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(DateTime dateTime) {
        this.dateTime = dateTime;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
}

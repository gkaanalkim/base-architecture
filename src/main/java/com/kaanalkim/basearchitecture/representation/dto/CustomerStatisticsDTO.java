package com.kaanalkim.basearchitecture.representation.dto;

public class CustomerStatisticsDTO {
    private Integer allCustomerNumber;
    private Integer deactiveCustomerNumber;
    private Integer activeCustomerNumber;

    public CustomerStatisticsDTO(Integer allCustomerNumber, Integer deactiveCustomerNumber, Integer activeCustomerNumber) {
        this.allCustomerNumber = allCustomerNumber;
        this.deactiveCustomerNumber = deactiveCustomerNumber;
        this.activeCustomerNumber = activeCustomerNumber;
    }

    public Integer getAllCustomerNumber() {
        return allCustomerNumber;
    }

    public void setAllCustomerNumber(Integer allCustomerNumber) {
        this.allCustomerNumber = allCustomerNumber;
    }

    public Integer getDeactiveCustomerNumber() {
        return deactiveCustomerNumber;
    }

    public void setDeactiveCustomerNumber(Integer deactiveCustomerNumber) {
        this.deactiveCustomerNumber = deactiveCustomerNumber;
    }

    public Integer getActiveCustomerNumber() {
        return activeCustomerNumber;
    }

    public void setActiveCustomerNumber(Integer activeCustomerNumber) {
        this.activeCustomerNumber = activeCustomerNumber;
    }
}

package com.kaanalkim.basearchitecture.representation.dto;

/**
 * Created by gunerkaanalkim on 09/09/15.
 */
public class TokenDTO {
    private String token;
    private Integer expireTime;

    public TokenDTO(String token, Integer expireTime) {
        this.token = token;
        this.expireTime = expireTime;
    }

    public Integer getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(Integer expireTime) {
        this.expireTime = expireTime;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}

package com.kaanalkim.basearchitecture.enums;

public enum MenuEnum {
    SYSTEM_MANAGENEMENT("SystemManagement"),
    AUTHENICATION_AUTHORIZATION("AuthenticationAuthorization"),
    COMPANY("Company");

    private String menuAlias;

    private MenuEnum(String menuAlias) {
        this.menuAlias = menuAlias;
    }

    public String getMenuAlias() {
        return this.menuAlias;
    }
}


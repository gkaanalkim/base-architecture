package com.kaanalkim.basearchitecture.enums;

public enum ErrorMessage {
    TR_DUPLICATE_CODE("9003"),
    TR_DUPLICATE_EMAIL("9004");

    private String errorAlias;

    private ErrorMessage(String errorAlias) {
        this.errorAlias = errorAlias;
    }

    public String getErrorAlias() {
        return errorAlias;
    }
}
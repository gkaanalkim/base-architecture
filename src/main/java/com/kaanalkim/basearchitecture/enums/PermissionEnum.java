package com.kaanalkim.basearchitecture.enums;

public enum PermissionEnum {
    ROLE_CREATE("RoleCreate"),
    ROLE_READ("RoleRead"),
    ROLE_UPDATE("RoleUpdate"),
    ROLE_DELETE("RoleDelete"),
    ROLE_ASSING("RoleAssing"),

    USER_CREATE("UserCreate"),
    USER_READ("UserRead"),
    USER_UPDATE("UserUpdate"),
    USER_DELETE("UserDelete"),

    MENU_READ("MenuRead"),
    MENU_DELETE("MenuDelete"),
    MENU_ASSING("MenuAssing"),
    MENU_ASSING_BACK("MenuAssingBack"),

    PROFILE_READ("ProfileRead"),
    PROFILE_UPDATE("ProfileUpdate"),

    PERMISSION_READ("PermissionRead"),
    PERMISSION_ASSING("PermissionAssing");

    private String permissionAlias;

    private PermissionEnum(String permissionAlias) {
        this.permissionAlias = permissionAlias;
    }

    public String getPermissionAlias() {
        return permissionAlias;
    }
}


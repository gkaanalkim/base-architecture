package com.kaanalkim.basearchitecture.wandservice;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface WandResource {
    String path();

    String name() default "";

    String alias();

    String description();

    String httpHeader() default "";

    String version() default "";

    String author() default "admin";
}

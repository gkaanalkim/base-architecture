package com.kaanalkim.basearchitecture.wandservice;

import com.kaanalkim.basearchitecture.representation.entities.Resource;
import io.dropwizard.Configuration;
import io.dropwizard.ConfiguredBundle;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.List;

public class WandResourceBundle<T extends Configuration> implements ConfiguredBundle<T> {
    HibernateBundle hibernateBundle;
    Session session;
    List<Class> resources;

    public WandResourceBundle(HibernateBundle hibernateBundle, List<Class> resources) {
        this.hibernateBundle = hibernateBundle;
        this.resources = resources;
    }

    public WandResourceBundle(Session session, List<Class> resources) {
        this.session = session;
        this.resources = resources;
    }

    @Override
    public void initialize(Bootstrap<?> bootstrap) {

    }

    @Override
    public void run(Configuration configuration, Environment environment) throws Exception {
        Session session = this.openSession(this.hibernateBundle, configuration, environment);

        for (Class clazz : this.resources) {
            Method[] methods = clazz.getMethods();

            for (Method method : methods) {
                Annotation[] annotations = method.getDeclaredAnnotations();

                for (Annotation annotation : annotations) {
                    if (annotation instanceof WandResource) {
                        WandResource wandResource = (WandResource) annotation;

                        String servicePath = wandResource.path();

                        Resource service = (Resource) session.createCriteria(Resource.class).add(Restrictions.eq("path", servicePath)).uniqueResult();

                        if (service == null) {
                            service = new Resource();
                            service.setAuthor(wandResource.author());
                            service.setName(wandResource.name());
                            service.setDescription(wandResource.description());
                            service.setPath(wandResource.path());
                            service.setVersion(wandResource.version());
                            service.setAlias(wandResource.alias());

                            session.save(service);
                        } else {
                            service.setAuthor(wandResource.author());
                            service.setName(wandResource.name());
                            service.setDescription(wandResource.description());
                            service.setPath(wandResource.path());
                            service.setVersion(wandResource.version());
                            service.setAlias(wandResource.alias());

                            session.update(service);
                        }
                    }
                }
            }
        }

        session.flush();
        session.close();
    }

    private Session openSession(HibernateBundle hibernateBundle, Configuration configuration, Environment environment) {
        SessionFactory sessionFactory = hibernateBundle.getSessionFactory();

        if (sessionFactory == null) {
            try {
                hibernateBundle.run(configuration, environment);
            } catch (Exception e) {
                //TODO
            }
        }

        return hibernateBundle.getSessionFactory().openSession();
    }
}

package com.kaanalkim.basearchitecture;

import com.kaanalkim.basearchitecture.authentication.WandAuthenticationBundle;
import com.kaanalkim.basearchitecture.authorization.WandAuthorizationBundle;
import com.kaanalkim.basearchitecture.cli.InitializeCommand;
import com.kaanalkim.basearchitecture.dao.dao.*;
import com.kaanalkim.basearchitecture.representation.entities.*;
import com.kaanalkim.basearchitecture.resource.*;
import com.kaanalkim.basearchitecture.wandservice.WandResourceBundle;
import io.dropwizard.Application;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;

public class App extends Application<BaseConfiguration> {
    private static final Logger LOGGER = LoggerFactory.getLogger(App.class);

    private final HibernateBundle<BaseConfiguration> hibernateBundle = new HibernateBundle<BaseConfiguration>(
            Menu.class,
            Role.class,
            User.class,
            SystemActivity.class,
            RoleMenu.class,
            RoleUser.class,
            UserToken.class,
            Resource.class,
            Authorization.class
    ) {
        @Override
        public DataSourceFactory getDataSourceFactory(BaseConfiguration baseConfiguration) {
            return baseConfiguration.getDatabase();
        }
    };

    private List wandServiceClasses = Arrays.asList(
            AuthResource.class,
            MenuResource.class,
            RoleResource.class,
            UserResource.class,
            ServiceResource.class
    );

    @Override
    public void initialize(Bootstrap<BaseConfiguration> bootstrap) {
        bootstrap.addBundle(this.hibernateBundle);
        bootstrap.addCommand(new InitializeCommand<>(this, hibernateBundle));

        bootstrap.addBundle(new WandAuthenticationBundle<BaseConfiguration>(hibernateBundle));
        bootstrap.addBundle(new WandAuthorizationBundle<BaseConfiguration>(hibernateBundle));
        bootstrap.addBundle(new WandResourceBundle<BaseConfiguration>(hibernateBundle, wandServiceClasses));
    }

    @Override
    public void run(BaseConfiguration configuration, Environment environment) throws Exception {
        LOGGER.info("Services are run.");

        //  CORS Settings
        FilterRegistration.Dynamic filter = environment.servlets().addFilter("CORS", CrossOriginFilter.class);
        filter.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "GET,PUT,POST,DELETE,OPTIONS");
        filter.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, "*");
        filter.setInitParameter(CrossOriginFilter.ACCESS_CONTROL_ALLOW_ORIGIN_HEADER, "*");
        filter.setInitParameter(CrossOriginFilter.ALLOWED_HEADERS_PARAM, "Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin");
        filter.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");
        //  CORS Settings

        final RoleDAO roleDAO = new RoleDAO(hibernateBundle.getSessionFactory());
        final RoleMenuDAO roleMenuDAO = new RoleMenuDAO(hibernateBundle.getSessionFactory());
        final RoleUserDAO roleUserDAO = new RoleUserDAO(hibernateBundle.getSessionFactory());
        final SystemActivityDAO systemActivityDAO = new SystemActivityDAO(hibernateBundle.getSessionFactory());
        final UserDAO userDAO = new UserDAO(hibernateBundle.getSessionFactory());
        final UserTokenDAO userTokenDAO = new UserTokenDAO(hibernateBundle.getSessionFactory());
        final MenuDAO menuDAO = new MenuDAO(hibernateBundle.getSessionFactory());
        final AuthorizationDAO authorizationDAO = new AuthorizationDAO(hibernateBundle.getSessionFactory());
        final ResourceDAO resourceDAO = new ResourceDAO(hibernateBundle.getSessionFactory());

        environment.jersey().register(new AuthResource(userDAO, userTokenDAO));
        environment.jersey().register(new MenuResource(userDAO, menuDAO, roleDAO, roleMenuDAO, roleUserDAO));
        environment.jersey().register(new RoleResource(roleDAO, userDAO, roleUserDAO));
        environment.jersey().register(new ServiceResource(resourceDAO, roleDAO, authorizationDAO));
    }

    public static void main(String[] args) throws Exception {
        new App().run(args);
    }
}
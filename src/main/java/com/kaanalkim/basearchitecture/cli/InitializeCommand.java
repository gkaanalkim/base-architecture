package com.kaanalkim.basearchitecture.cli;

import com.kaanalkim.basearchitecture.BaseConfiguration;
import com.kaanalkim.basearchitecture.enums.MenuEnum;
import com.kaanalkim.basearchitecture.representation.entities.*;
import io.dropwizard.Application;
import io.dropwizard.Configuration;
import io.dropwizard.cli.EnvironmentCommand;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.hibernate.UnitOfWork;
import io.dropwizard.setup.Environment;
import net.sourceforge.argparse4j.inf.Namespace;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by gunerkaanalkim on 10/01/18.
 */
public class InitializeCommand<T extends BaseConfiguration> extends EnvironmentCommand<T> {
    private static final Logger LOGGER = LoggerFactory.getLogger(InitializeCommand.class);
    private HibernateBundle hibernateBundle;

    public InitializeCommand(Application application, HibernateBundle hibernateBundle) {
        super(application, "initialize", "Runs initial command for initial process.");
        this.hibernateBundle = hibernateBundle;
    }

    @Override
    protected void run(Environment environment, Namespace namespace, T t) throws Exception {
        LOGGER.info("Initialize Starting...");
        LOGGER.info("Starting to create initial data.");
        execute(t);
    }

    @UnitOfWork
    public void execute(Configuration configuration) {
        final Session session = hibernateBundle.getSessionFactory().openSession();

        this.createMenu(session);
        this.createSuperUser(session);
        session.close();
    }

    public void createMenu(Session session) {
        int counter = 0;

        Menu userMainMenu = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("code", MenuEnum.SYSTEM_MANAGENEMENT.getMenuAlias())).uniqueResult();

        if (userMainMenu == null) {
            userMainMenu = new Menu();
            userMainMenu.setName("Sistem Yönetimi");
            userMainMenu.setCode(MenuEnum.SYSTEM_MANAGENEMENT.getMenuAlias());
            userMainMenu.setDescription("Sistem yönetimi sayfasıdır.");
            userMainMenu.setMenuOrder(counter);
            session.persist(userMainMenu);
        } else {
            userMainMenu.setName("Sistem Yönetimi");
            userMainMenu.setCode(MenuEnum.SYSTEM_MANAGENEMENT.getMenuAlias());
            userMainMenu.setDescription("Sistem yönetimi sayfasıdır.");
            userMainMenu.setMenuOrder(counter);
            session.persist(userMainMenu);
        }
        counter++;

        Menu authenticationAuthorization = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("code", MenuEnum.AUTHENICATION_AUTHORIZATION.getMenuAlias())).uniqueResult();

        if (authenticationAuthorization == null) {
            authenticationAuthorization = new Menu();
            authenticationAuthorization.setName("Kimliklendirme & Yetkilendirme");
            authenticationAuthorization.setCode(MenuEnum.AUTHENICATION_AUTHORIZATION.getMenuAlias());
            authenticationAuthorization.setDescription("Kullanıcı işlemleri sayfasıdır.");
            authenticationAuthorization.setUrl("kimliklendirme-yetkilendirme.html");
            authenticationAuthorization.setParentOid(userMainMenu.getOid());
            authenticationAuthorization.setMenuOrder(counter);
            session.persist(authenticationAuthorization);
        } else {
            authenticationAuthorization.setName("Kimliklendirme & Yetkilendirme");
            authenticationAuthorization.setCode(MenuEnum.AUTHENICATION_AUTHORIZATION.getMenuAlias());
            authenticationAuthorization.setDescription("Kullanıcı işlemleri sayfasıdır.");
            authenticationAuthorization.setUrl("kimliklendirme-yetkilendirme.html");
            authenticationAuthorization.setParentOid(userMainMenu.getOid());
            authenticationAuthorization.setMenuOrder(counter);
            session.persist(authenticationAuthorization);
        }
        counter++;

        Menu company = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("code", MenuEnum.COMPANY.getMenuAlias())).uniqueResult();

        if (company == null) {
            company = new Menu();
            company.setName("Firma Yönetimi");
            company.setCode(MenuEnum.COMPANY.getMenuAlias());
            company.setDescription("Firma oluşturma sayfasıdır.");
            company.setUrl("firma.html");
            company.setParentOid(userMainMenu.getOid());
            company.setMenuOrder(counter);
            session.persist(company);
        } else {
            company.setName("Firma Yönetimi");
            company.setCode(MenuEnum.COMPANY.getMenuAlias());
            company.setDescription("Firma oluşturma sayfasıdır.");
            company.setUrl("firma.html");
            company.setParentOid(userMainMenu.getOid());
            company.setMenuOrder(counter);
            session.persist(company);
        }

        session.flush();
    }

    public void createSuperUser(Session session) {
        User superUser = (User) session.createCriteria(User.class).add(Restrictions.eq("code", "SA")).uniqueResult();

        if (superUser == null) {
            superUser = new User();
            superUser.setName("Güner Kaan");
            superUser.setSurname("ALKIM");
            superUser.setCode("SA");
            superUser.setMobilePhone("05418847672");
            superUser.setInternalPhoneCode("0000");
            superUser.setUsername("admin");
            superUser.setPassword("03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4");

            session.persist(superUser);
        } else {
            superUser.setName("Güner Kaan");
            superUser.setSurname("ALKIM");
            superUser.setCode("SA");
            superUser.setMobilePhone("05418847672");
            superUser.setInternalPhoneCode("0000");
            superUser.setUsername("admin");
            superUser.setPassword("03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4");

            session.persist(superUser);
        }

        Role superAdminRole = (Role) session.createCriteria(Role.class).add(Restrictions.eq("code", "SAR")).uniqueResult();

        if (superAdminRole == null) {
            superAdminRole = new Role();
            superAdminRole.setCode("SAR");
            superAdminRole.setName("Super Admin Role");

            session.persist(superAdminRole);
        } else {
            superAdminRole.setCode("SAR");
            superAdminRole.setName(" Super Admin Role");

            session.persist(superAdminRole);
        }

        List<Resource> resources = session.createCriteria(Resource.class).list();

        for (Resource resource : resources) {
            Authorization authorization = (Authorization) session.createCriteria(Authorization.class)
                    .add(Restrictions.eq("role.oid", superAdminRole.getOid()))
                    .add(Restrictions.eq("resource.oid", resource.getOid()))
                    .uniqueResult();

            if (authorization == null) {
                authorization = new Authorization();
                authorization.setResource(resource);
                authorization.setRole(superAdminRole);

                session.persist(authorization);
            }
        }

        List<Menu> menus = session.createCriteria(Menu.class).list();

        for (Menu menu : menus) {
            RoleMenu roleMenu = (RoleMenu) session.createCriteria(RoleMenu.class)
                    .add(Restrictions.eq("role.oid", superAdminRole.getOid()))
                    .add(Restrictions.eq("menu.oid", menu.getOid()))
                    .uniqueResult();

            if (roleMenu == null) {
                roleMenu = new RoleMenu();
                roleMenu.setMenu(menu);
                roleMenu.setRole(superAdminRole);

                session.persist(roleMenu);
            }
        }

        RoleUser superAdminRoleUser = (RoleUser) session.createCriteria(RoleUser.class)
                .add(Restrictions.eq("role.oid", superAdminRole.getOid()))
                .add(Restrictions.eq("user.oid", superUser.getOid()))
                .uniqueResult();

        if (superAdminRoleUser == null) {
            superAdminRoleUser = new RoleUser();
            superAdminRoleUser.setRole(superAdminRole);
            superAdminRoleUser.setUser(superUser);

            session.persist(superAdminRoleUser);
        }


        session.flush();
    }
}

package com.kaanalkim.basearchitecture.resource;

import com.kaanalkim.basearchitecture.authentication.WandAuthentication;
import com.kaanalkim.basearchitecture.authorization.WandAuthorization;
import com.kaanalkim.basearchitecture.dao.dao.UserDAO;
import com.kaanalkim.basearchitecture.enums.ErrorMessage;
import com.kaanalkim.basearchitecture.representation.dto.UserDTO;
import com.kaanalkim.basearchitecture.representation.entities.User;
import com.kaanalkim.basearchitecture.wandservice.WandResource;
import io.dropwizard.hibernate.UnitOfWork;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.util.Map;

/**
 * Created by gunerkaanalkim on 10/09/15.
 */
@Path("user")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UserResource {
    UserDAO userDAO;

    public UserResource(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @GET
    @Path("read")
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(name = "userRead", alias = "userRead", description = "Tüm kullanıcıları görüntüleyebilir.", author = "Kaan ALKIM", path = "/user/read", version = "1.0.0")
    public Response read() {
        return Response.ok(userDAO.findAll(User.class)).build();
    }

    @GET
    @Path("read/{oid}")
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(name = "readByOid", alias = "readByOid", description = "ID eşleşen kullanıcıyı görüntüleyebilir.", author = "Kaan ALKIM", path = "read/{oid}", version = "1.0.0")
    public Response readByOid(@PathParam("oid") String oid) {
        return Response.ok(userDAO.findByOid(oid)).build();
    }

    @GET
    @Path("whoAmI")
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(name = "whoAmI", alias = "whoAmI", description = "Kişisel bilgilerini görüntüleyebilir.", author = "Kaan ALKIM", path = "/user/whoAmI", version = "1.0.0")
    public Response whoAmI(@Context SecurityContext securityContext) {
        User user = userDAO.findByUsername(securityContext.getUserPrincipal().getName());

        return Response.ok().entity(user).build();
    }

    @POST
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(name = "userCreate", alias = "userCreate", description = "Bir kullanıcı kaydedebilir.", author = "Kaan ALKIM", path = "/user/create", version = "1.0.0")
    public Response create(Map<String, String> staffData) {
        String code = staffData.get("code");
        String username = staffData.get("username");

        User user = userDAO.getByCode(code);

        if (user == null) {

            user = userDAO.findByUsername(username);

            if (user == null) {

                String name = staffData.get("name");
                String surname = staffData.get("surname");
                String mobilePhone = staffData.get("mobilePhone");
                String internalPhoneCode = staffData.get("internalPhoneCode");

                String password = staffData.get("password");

                user = new User();
                user.setName(name);
                user.setSurname(surname);
                user.setCode(code);
                user.setMobilePhone(mobilePhone);
                user.setInternalPhoneCode(internalPhoneCode);
                user.setUsername(username);
                user.setPassword(password);

                userDAO.create(user);

                return Response.ok().entity(user).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_EMAIL.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_CODE.getErrorAlias()).build();
        }
    }

    @PUT
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(name = "userUpdate", alias = "userUpdate", description = "Bir kullanıcı güncelleyebilir.", author = "Kaan ALKIM", path = "/user/update", version = "1.0.0")
    public Response update(Map<String, String> userData) {
        String oldUserOid = userData.get("userOid");
        String name = userData.get("name");
        String surname = userData.get("surname");
        String mobilePhone = userData.get("mobilePhone");
        String code = userData.get("code");
        String internalPhoneCode = userData.get("internalPhoneCode");
        String username = userData.get("username");

        User user = userDAO.findByOid(oldUserOid);
        user.setName(name);
        user.setSurname(surname);
        user.setCode(code);
        user.setMobilePhone(mobilePhone);
        user.setInternalPhoneCode(internalPhoneCode);
        user.setUsername(username);
        user.setPassword(user.getPassword());

        userDAO.update(user);

        return Response.ok().entity(user).build();
    }

    @DELETE
    @Path("{oid}")
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(name = "userDelete", alias = "userDelete", description = "Bir kullanıcı silebilir.", author = "Kaan ALKIM", path = "/user/delete", version = "1.0.0")
    public Response delete(@PathParam("oid") String oid) {
        User user = userDAO.findByOid(oid);

        userDAO.delete(user);

        return Response.ok(user).build();
    }

    @POST
    @Path("getProfile")
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(name = "getProfile", alias = "Kendi Profilini Görüntüler", description = "Profil görüntüleyebilir.", author = "Kaan ALKIM", path = "/getProfile", version = "1.0.0")
    public Response getProfile(@Context SecurityContext securityContext) {
        String username = securityContext.getUserPrincipal().getName();

        User user = userDAO.findByUsername(username);

        UserDTO userDTO = new UserDTO();
        userDTO.setDeleteStatus(user.getDeleteStatus());
        userDTO.setName(user.getName());
        userDTO.setSurname(user.getSurname());
        userDTO.setInternalPhoneCode(user.getInternalPhoneCode());
        userDTO.setMobilePhone(user.getMobilePhone());
        userDTO.setEmail(user.getUsername());
        userDTO.setPassword(user.getPassword());

        return Response.ok(userDTO).build();
    }
}
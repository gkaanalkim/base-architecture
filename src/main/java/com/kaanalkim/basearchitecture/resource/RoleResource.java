package com.kaanalkim.basearchitecture.resource;

import com.kaanalkim.basearchitecture.authentication.WandAuthentication;
import com.kaanalkim.basearchitecture.authorization.WandAuthorization;
import com.kaanalkim.basearchitecture.dao.dao.RoleDAO;
import com.kaanalkim.basearchitecture.dao.dao.RoleUserDAO;
import com.kaanalkim.basearchitecture.dao.dao.UserDAO;
import com.kaanalkim.basearchitecture.enums.ErrorMessage;
import com.kaanalkim.basearchitecture.representation.entities.Role;
import com.kaanalkim.basearchitecture.representation.entities.RoleUser;
import com.kaanalkim.basearchitecture.representation.entities.User;
import com.kaanalkim.basearchitecture.wandservice.WandResource;
import io.dropwizard.hibernate.UnitOfWork;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.util.List;
import java.util.Map;

/**
 * Created by gunerkaanalkim on 07/09/15.
 */
@Path("role")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class RoleResource {
    RoleDAO roleDAO;
    UserDAO userDAO;
    RoleUserDAO roleUserDAO;

    public RoleResource(RoleDAO roleDAO, UserDAO userDAO, RoleUserDAO roleUserDAO) {
        this.roleDAO = roleDAO;
        this.userDAO = userDAO;
        this.roleUserDAO = roleUserDAO;
    }

    @POST
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(name = "roleCreate", alias = "roleCreate", description = "Bir rol oluşturabilir.", author = "Kaan ALKIM", path = "/role/create", version = "1.0.0")
    public Response create(Map<String, String> roleData) {
        String name = roleData.get("name");
        String code = roleData.get("code");

        Role role = roleDAO.getByCode(code);

        if (role == null) {
            role = new Role();

            role.setName(name);
            role.setCode(code);
            roleDAO.create(role);
            return Response.ok().entity(role).build();
        } else {
            return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_CODE.getErrorAlias()).build();
        }
    }

    @GET
    @Path("read")
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(name = "roleRead", alias = "roleRead", description = "Tüm rolleri görüntüleyebilir.", author = "Kaan ALKIM", path = "/role/read", version = "1.0.0")
    public Response read(Map<String, String> roleData, @Context SecurityContext securityContext) {
        User user = userDAO.findByUsername(securityContext.getUserPrincipal().getName());

        List<Role> roles;

        if (user.getCode().equals("SAU")) {
            roles = roleDAO.findAll(Role.class);
        } else {
            roles = roleDAO.getAllExceptString(Role.class, "SAUADMIN");
        }

        return Response.ok(roles).build();
    }

    @GET
    @Path("getRoleByUser/{oid}")
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(name = "getRoleByUser", alias = "getRoleByUser", description = "Bir kullanıcının rolünü görüntüleyebilir.", author = "Kaan ALKIM", path = "/role/getRoleByUser", version = "1.0.0")
    public Response getRoleByUser(@PathParam("oid") String oid) {
        User user = userDAO.findByOid(oid);

        RoleUser roleUser = roleUserDAO.getByUser(user);

        return Response.ok(roleUser == null ? new Role() : roleUser.getRole()).build();
    }

    @GET
    @Path("read/{oid}")
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(name = "readByOid", alias = "readByOid", description = "Bir rol görüntüleyebilir.", author = "Kaan ALKIM", path = "/role/readByOid", version = "1.0.0")
    public Response getByOid(@PathParam("oid") String oid) {
        return Response.ok().entity(roleDAO.getById(oid)).build();
    }

    @PUT
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(name = "roleUpdate", alias = "roleUpdate", description = "Bir rol güncelleyebilir.", author = "Kaan ALKIM", path = "/role/update", version = "1.0.0")
    public Response update(Map<String, String> roleData) {
        String oid = roleData.get("oid");
        String name = roleData.get("name");
        String code = roleData.get("code");

        Role role = roleDAO.getById(oid);

        String oldCode = roleDAO.getById(oid).getCode();
        Role codeControl = roleDAO.getByCode(code);

        if ((codeControl != null) && (codeControl.getCode() != oldCode)) {
            return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_CODE.getErrorAlias()).build();
        }

        if (!role.getCode().equals("Admin")) {
            role.setName(name);
            role.setCode(code);

            roleDAO.update(role);

            return Response.ok(role).build();
        } else {
            return Response.ok("Bu bir sistem rolüdür, güncelleyemezsiniz.").build();
        }
    }

    @DELETE
    @Path("{oid}")
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(name = "roleDelete", alias = "roleDelete", description = "Bir rol silebilir.", author = "Kaan ALKIM", path = "/role/delete", version = "1.0.0")
    public Response delete(@PathParam("oid") String oid) {
        Role role = roleDAO.getById(oid);

        if (!role.getCode().equals("Admin")) {
            roleDAO.delete(role);

            return Response.ok(role).build();
        } else {
            return Response.ok("Bu bir sistem rolüdür, silemezsiniz.").build(); //TODO
        }
    }

    @POST
    @UnitOfWork
    @Path("assignRoleToUser")
    @WandAuthentication
    @WandAuthorization
    @WandResource(name = "assignRoleToUser", alias = "assignRoleToUser", description = "Bir rolü bir kullanıcıya atayabilir.", author = "Kaan ALKIM", path = "/role/assignRoleToUser", version = "1.0.0")
    public Response assingroletouser(Map<String, String> data) {
        String roleOid = data.get("role");
        String userOid = data.get("user");

        User user = userDAO.findByOid(userOid);
        Role role = roleDAO.getById(roleOid);

        RoleUser roleUser = roleUserDAO.getByUser(user);

        if (roleUser == null) {
            roleUser = new RoleUser();
            roleUser.setRole(role);
            roleUser.setUser(user);

            roleUserDAO.create(roleUser);

            return Response.ok(roleUser).build();
        } else {
            roleUser.setRole(role);

            roleUserDAO.update(roleUser);

            return Response.ok(roleUser).build();
        }
    }
}

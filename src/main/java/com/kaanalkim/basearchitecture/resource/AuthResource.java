package com.kaanalkim.basearchitecture.resource;

import com.kaanalkim.basearchitecture.authentication.Credentials;
import com.kaanalkim.basearchitecture.authentication.TokenGenerator;
import com.kaanalkim.basearchitecture.authentication.WandAuthentication;
import com.kaanalkim.basearchitecture.dao.dao.UserDAO;
import com.kaanalkim.basearchitecture.dao.dao.UserTokenDAO;
import com.kaanalkim.basearchitecture.representation.dto.TokenDTO;
import com.kaanalkim.basearchitecture.representation.entities.User;
import com.kaanalkim.basearchitecture.representation.entities.UserToken;
import io.dropwizard.hibernate.UnitOfWork;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Map;

/**
 * Created by gunerkaanalkim on 09/09/15.
 */
@Path("auth")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class AuthResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthResource.class);

    UserDAO userDAO;
    UserTokenDAO userTokenDAO;

    public AuthResource(UserDAO userDAO, UserTokenDAO userTokenDAO) {
        this.userDAO = userDAO;
        this.userTokenDAO = userTokenDAO;
    }

    @POST
    @Path("login")
    @UnitOfWork
    public Response login(Credentials credentials) {
        User user = userDAO.findByUsername(credentials.getUsername());

        if (user == null) {
            LOGGER.error("Unknown User : {} ", credentials.toString());

            throw new WebApplicationException(Response.Status.UNAUTHORIZED);
        } else if (user.getPassword().equals(credentials.getPassword())) {
            UserToken userToken = userTokenDAO.getByUser(user);

            if (userToken != null) {
                userTokenDAO.delete(userToken);
            }

            TokenDTO tokenDTO = new TokenDTO(TokenGenerator.generate().toString(), 5);

            userToken = new UserToken();
            userToken.setUser(user);
            userToken.setStatus(true);
            userToken.setToken(tokenDTO.getToken());

            userTokenDAO.create(userToken);

            return Response.ok().entity(tokenDTO).build();
        } else {
            LOGGER.error("Unauthorize User : {} ", credentials.toString());
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
    }

    @POST
    @Path("logout")
    @UnitOfWork
    @WandAuthentication
    public Response logout(Map<String, String> tokenData) {
        String token = tokenData.get("token");

        UserToken userToken = userTokenDAO.findByToken(token);
        userTokenDAO.delete(userToken);

        return Response.ok().build();
    }
}

package com.kaanalkim.basearchitecture.authentication;

import java.util.UUID;

public class TokenGenerator {
    public static UUID generate() {
        return UUID.randomUUID();
    }

    public static UUID generateByString(String salt) {
        return UUID.randomUUID().fromString(salt);
    }
}
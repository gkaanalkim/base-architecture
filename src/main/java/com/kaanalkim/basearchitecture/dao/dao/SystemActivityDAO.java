package com.kaanalkim.basearchitecture.dao.dao;

import com.kaanalkim.basearchitecture.dao.core.BaseDAO;
import com.kaanalkim.basearchitecture.representation.entities.SystemActivity;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;

import java.util.List;

public class SystemActivityDAO extends BaseDAO<SystemActivity> {
    public SystemActivityDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<SystemActivity> getAll() {
        Criteria criteria = currentSession().createCriteria(SystemActivity.class);
        criteria.addOrder(Order.desc("creationDateTime"))
                .setMaxResults(5000);

        return list(criteria);
    }

    public List<SystemActivity> getByDateRange(DateTime start, DateTime end) {
        Criteria criteria = currentSession().createCriteria(SystemActivity.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.between("creationDateTime", start, end))
                .addOrder(Order.desc("creationDateTime"));

        return list(criteria);
    }
}


package com.kaanalkim.basearchitecture.dao.dao;

import com.kaanalkim.basearchitecture.dao.core.BaseDAO;
import com.kaanalkim.basearchitecture.representation.entities.Menu;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.List;

public class MenuDAO extends BaseDAO<Menu> {
    public MenuDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Menu getByCode(String code) {
        Criteria criteria = currentSession().createCriteria(Menu.class);
        criteria.add(Restrictions.eq("code", code));
        return uniqueResult(criteria);
    }

    public List<Menu> getAll(Boolean deleteStatus) {
        Criteria criteria = currentSession().createCriteria(Menu.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("deleteStatus", deleteStatus));

        return list(criteria);
    }

    public List<Menu> getAll() {
        Criteria criteria = currentSession().createCriteria(Menu.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .addOrder(Order.asc("menuOrder"));

        return list(criteria);
    }

    public Menu getById(String oid) {
        Criteria criteria = currentSession().createCriteria(Menu.class);
        criteria.add(Restrictions.eq("deleteStatus", false)).add(Restrictions.eq("oid", oid));

        return uniqueResult(criteria);
    }

    public Menu getByIdAndProject(String oid) {
        Criteria criteria = currentSession().createCriteria(Menu.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("oid", oid));

        return uniqueResult(criteria);
    }

    public Menu delete(Menu entity) {
        entity.setDeleteStatus(true);

        return super.persist(entity);
    }

    public Menu getByUrl(String url) {
        Criteria criteria = currentSession().createCriteria(Menu.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("url", url));

        return uniqueResult(criteria);
    }


    public List<Menu> getByCodeContains(String project, String code) {
        Criteria criteria = currentSession().createCriteria(Menu.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.like("code", code, MatchMode.ANYWHERE));

        return list(criteria);
    }

}

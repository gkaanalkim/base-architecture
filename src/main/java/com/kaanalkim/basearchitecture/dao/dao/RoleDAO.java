package com.kaanalkim.basearchitecture.dao.dao;

import com.kaanalkim.basearchitecture.dao.core.BaseDAO;
import com.kaanalkim.basearchitecture.representation.entities.Role;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

public class RoleDAO extends BaseDAO<Role> {
    public RoleDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Role findByCode(String code) {
        Criteria criteria = currentSession().createCriteria(Role.class);
        criteria.add(Restrictions.eq("code", code)).add(Restrictions.eq("deleteStatus", false));

        return uniqueResult(criteria);
    }

    @Override
    public List<Role> findAll(Class<Role> t) {
        Criteria criteria = currentSession().createCriteria(t);
        criteria.add(Restrictions.eq("deleteStatus", false));

        return list(criteria);
    }

    @Override
    public Role findById(String oid) {
        Criteria criteria = currentSession().createCriteria(Role.class);
        criteria.add(Restrictions.eq("deleteStatus", false)).add(Restrictions.eq("oid", oid));

        return uniqueResult(criteria);
    }

    @Override
    public Role delete(Role entity) {
        entity.setDeleteStatus(true);

        return super.persist(entity);
    }

    public List<Role> getAllExceptString(Class<Role> t, String adminRoleCode) {
        Criteria criteria = currentSession().createCriteria(t);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.ne("code", adminRoleCode));
        return list(criteria);
    }

    public Role getById(String oid) {
        Criteria criteria = currentSession().createCriteria(Role.class);
        criteria.add(Restrictions.eq("deleteStatus", false)).add(Restrictions.eq("oid", oid));

        return uniqueResult(criteria);
    }

    public Role getByCode(String code) {
        Criteria criteria = currentSession().createCriteria(Role.class);
        criteria.add(Restrictions.eq("deleteStatus", false)).add(Restrictions.eq("code", code));

        return uniqueResult(criteria);
    }
}


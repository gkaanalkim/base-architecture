package com.kaanalkim.basearchitecture.dao.dao;

import com.kaanalkim.basearchitecture.dao.core.BaseDAO;
import com.kaanalkim.basearchitecture.representation.entities.Authorization;
import com.kaanalkim.basearchitecture.representation.entities.Resource;
import com.kaanalkim.basearchitecture.representation.entities.Role;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

public class AuthorizationDAO extends BaseDAO<Authorization> {
    public AuthorizationDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<Authorization> getByRole(Role role) {
        Criteria criteria = currentSession().createCriteria(Authorization.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("role.oid", role.getOid()));

        return list(criteria);
    }

    public Authorization findByRoleAndResource(Role role, Resource resource){
        Criteria criteria = currentSession().createCriteria(Authorization.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("role.oid", role.getOid()))
                .add(Restrictions.eq("resource.oid", resource.getOid()));

        return uniqueResult(criteria);
    }
}

package com.kaanalkim.basearchitecture.dao.dao;

import com.kaanalkim.basearchitecture.dao.core.BaseDAO;
import com.kaanalkim.basearchitecture.representation.entities.Resource;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

public class ResourceDAO extends BaseDAO<Resource> {
    public ResourceDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Resource findByOid(String oid) {
        Criteria criteria = currentSession().createCriteria(Resource.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("oid", oid));

        return uniqueResult(criteria);
    }

    public List<Resource> findAll() {
        Criteria criteria = currentSession().createCriteria(Resource.class);
        criteria.add(Restrictions.eq("deleteStatus", false));

        return list(criteria);
    }
}
